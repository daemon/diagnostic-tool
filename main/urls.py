from django.urls import path
from main import views

urlpatterns=[
    path('',views.index,name='main-index'),
    path('index/',views.index2,name='sec-index'),
    path('login/',views.login,name='main-login'),
    path('register/',views.register,name='main-register'),
    path('dash/',views.dash,name='main-dash'),
    path('login2/',views.login2,name='sec-login'),
]
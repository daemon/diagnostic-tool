from django.shortcuts import render

# Create your views here.
def login(request):
    return render(request,'main/login.html')
def register(request):
    return render(request,'main/register.html')
def dash(request):
    return render(request,'main/dash.html')
def login2(request):
    return render(request,'main/login2.html')
def index(request):
    return render(request,'main/index.html')
def index2(request):
    return render(request,'main/index2.html')